import React from 'react';
import Layout from '../components/Layout';

export const About = (): JSX.Element => {
  return (
    <Layout
      customMeta={{
        title: 'About - Martin Crockett',
      }}
    >
      <h1>About</h1>

      <h2>Certification</h2>
      <ul className="list-disc pl-4 my-6">
        <li>
          FA Level 1, FA Level 2, FA Youth Award Module 1, FA Goalkeeping
          Coaching Award Level 1
        </li>
        <li className="mt-2">USSF US Soccer National F License</li>
        <li className="mt-2">
          NSCAA National Soccer Coaches Association of America Level 3 Diploma
        </li>
      </ul>
      <p>
        Performed as an assistant at a skills-focussed soccer school under the
        guidance of other experienced coaches.
      </p>
      <p>
        Became a certified coach in 2010, and continuously improved my studies
        through multiple coaching education angles and peer review.
      </p>
      <p>
        Worked with a number of talented young players who progressed to
        training and signing with development centers/academies at English
        Premier League and Championship clubs.
      </p>
      <p>
        Advanced my experience through working at the grassroots club level,
        gathering coaching experience within the youth set-up of a travel team
        in the seventh tier of the English football league system.
      </p>
      <p>Currently progressing towards UEFA B License level.</p>
      <p>
        Background checked by NYPD, NYC DOE, and FA Enhanced CRB checked. Hold
        clean, valid US and UK car driving licenses.
      </p>
      <p>
        Outside of soccer, extensive training in mixed-martial arts (MMA),
        specializations in Muay Thai, Boxing, and Brazilian jiu-jitsu.
        Passionate distance runner with close to 100 races finished.
      </p>
    </Layout>
  );
};

export default About;
